import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions } from 'react-native';

const DEVICE = Dimensions.get('window')

export default class AboutScreen extends React.Component{

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.headerTitle}>About us</Text>
                <Image style={styles.aboutImage} source={require(('../assets/corona.png'))}/>
                <Text style={styles.descAbout}>
                    Aplikasi ini merupakan aplikasi yang saya buat untuk
                    memenuhi Final Project dari Course SanberCode React-Native.
                    Fungsi dari aplikasi ini adalah untuk melakukan track pada status
                    Corona di Indonesia, aplikasi ini juga dapat menampilkan data per Provinsinya.
                    Sumber data didapatkan dari website https://kawalcorona.com/. Selain itu Aplkasi ini
                    membuat berita seputar corona di dunia. Saya harap aplikasi ini dapat bermanfaat @mattjevaas
                </Text>
                <TouchableOpacity style={styles.buttonNext} onPress={()=>{this.props.navigation.push('Main')}}>
                    <Text style={styles.buttonText} >Next &gt;</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 25,
        justifyContent: 'center'
    },
    headerTitle: {
        fontWeight: 'bold',
        fontSize: 30
    },
    aboutImage: {
        alignSelf: 'center',
        width: DEVICE.width * 0.8,
        height: DEVICE.width * 0.8
    },
    descAbout: {
        textAlign: 'justify'
    },
    buttonNext: {
        backgroundColor: '#3DB9FF',
        borderRadius: 4,
        height: DEVICE.height * 0.05,
        width: DEVICE.width * 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#3DB9FF',
        shadowOpacity: 0.5,
        shadowOffset: {width:5,height:5},
        alignSelf: 'flex-end',
        marginVertical: 50
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold'
    }
})