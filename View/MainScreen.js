import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions, Picker } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import RNPickerSelect from 'react-native-picker-select';
import Axios from 'axios';
import {FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILED} from '../src/constants/actionTypes'
import AnimateNumber from 'react-native-countup'

import Box from '../component/Box'

const DEVICE = Dimensions.get('window')
const  Separator = () => (
    <View style={styles.itemSeparator}/>
)

export default class MainScreen extends React.Component{

    constructor(props){
        super(props),
        this.state = {
            value: null,
            provData: {
                Kasus_Meni: 0,
                Kasus_Posi: 0,
                Kasus_Semb: 0,
            }
        }
    }

    async fetchData(){
        try{
            this.props.dispatch({
                type: FETCH_REQUEST
            })
            const data = await Axios.get("https://api.kawalcorona.com/indonesia")
            this.props.dispatch({
                type: FETCH_SUCCESS,
                payload: data.data
            })
        }catch(err){
            this.props.dispatch({
                type: FETCH_FAILED,
                payload: err
            })
            console.log(err)
        }
    }

    async fetchDataProvinsi(){
        try{
            const dataProvinsi = await Axios.get("https://api.kawalcorona.com/indonesia/provinsi")
            this.setState({value: dataProvinsi.data})
        }catch(err){
            console.log(err)
        }
    }

    findValue(value = null, prov = null){

        if (value!=null && prov != null){
            for(let i = 0; i < value.length; i++){
                if (value[i].attributes.Provinsi == prov){
                    this.setState({provData: value[i].attributes})
                }
                
            }
        }else{
            this.setState({provData: {Kasus_Meni: 0, Kasus_Posi: 0, Kasus_Semb: 0}})
        }

    }

    componentDidMount() {
        this.fetchData()
        this.fetchDataProvinsi()
    }

    render(){

        let positif,sembuh,dirawat,meninggal;
        positif = sembuh = dirawat = meninggal = 0

        const {corona} = this.props
        let dataRaw = corona.coronaData[0]

        if(dataRaw != undefined){
            positif = dataRaw.positif
            sembuh = dataRaw.sembuh
            dirawat = dataRaw.dirawat
            meninggal = dataRaw.meninggal
        }
        
        let arrProvinsi = []

        if(this.state.value != null){

            for(let i = 0; i < this.state.value.length; i++){
                let newObj = {label: this.state.value[i].attributes.Provinsi, value: this.state.value[i].attributes.Provinsi }
                arrProvinsi.push(newObj)
            }
        }

        return(
            <View style={styles.container}>
                <Text style={styles.headerTitle}>Update <Text style={styles.innerTitle}>Corona</Text> Indonesia</Text>
                <View style={styles.boxContainer1}>
                    <View style={styles.boxContainer2}>
                        <Box title='Positif' iconName='plus' iconColor='red' number={positif} />
                        <Box title='Sembuh' iconName='human-handsup' iconColor='#01BC79' number={sembuh}/>
                    </View>
                    <View style={styles.boxContainer2}>
                        <Box title='Meninggal' iconName='close' iconColor='#8C8C8C' number={meninggal}/>
                        <Box title='Dirawat' iconName='hospital-box' iconColor='#FDC80E' number={dirawat}/>
                    </View>
                </View>
                <View>
                    <Text style={styles.header2}>Detail Provinsi</Text>
                    <View style={styles.pickerContainer}>
                        <RNPickerSelect
                            useNativeAndroidPickerStyle={false}
                            onValueChange={(value) => {this.findValue(this.state.value,value)}}
                            items={arrProvinsi}
                            placeholder={{
                                label: 'Pilih Provinsi ...',
                                value: null,
                            }}
                            style={{...pickerSelectStyles}}
                            Icon={()=>{return <Icon name='chevron-down' size={18} style={{top: DEVICE.height * 0.01, right: DEVICE.width * 0.01}}  />}}
                        />
                    </View>
                    <View>
                        <View style={styles.descContainer}>
                            <Text style={styles.descText1}>Positif</Text>
                            <AnimateNumber style={styles.descText2} countBy={100} value={this.state.provData.Kasus_Posi} interval={1} />
                        </View>
                        <Separator/>
                        <View style={styles.descContainer}>
                            <Text style={styles.descText1}>Sembuh</Text>
                            <AnimateNumber style={styles.descText2} countBy={100} value={this.state.provData.Kasus_Semb} interval={1} />
                        </View>
                        <Separator/>
                        <View style={styles.descContainer}>
                            <Text style={styles.descText1}>Meninggal</Text>
                            <AnimateNumber style={styles.descText2} countBy={100} value={this.state.provData.Kasus_Meni} interval={1} />
                        </View>
                        <Separator/>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 40,
        paddingHorizontal: 20
    },
    itemSeparator:{
        borderColor: 'black',
        borderWidth: 0.5,
        marginVertical: 10
    },
    headerTitle:{
        fontWeight: 'bold',
        fontSize: 28,
        textAlign: 'center',
    },
    innerTitle:{
        color: 'red'
    },
    boxContainer1: {
        margin : 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    boxContainer2: {
        flexDirection: 'row',
    },
    box: {
        marginVertical: 20,
        marginHorizontal: 30,
        backgroundColor: 'white',
        borderRadius: 9,
        shadowColor: 'black',
        shadowOpacity: 0.2,
        shadowOffset: {width:1,height:1},
        width: DEVICE.width * 0.28,
        height: DEVICE.height * 0.15,

    },
    header2: {
        fontWeight: 'bold',
        fontSize: 20
    },
    descContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    descText2: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    descText1: {
        fontSize: 15,
    },
    pickerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 30
    }
})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: '#208AEC',
        borderRadius: DEVICE.width * 0.2,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        width: DEVICE.width * 0.9,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderWidth: 0.5,
        borderColor: '#208AEC',
        borderRadius: DEVICE.width * 0.2,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});