import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions } from 'react-native';
import TextInput from 'react-native-textinput-with-icons';

const DEVICE = Dimensions.get('window')

export default class LoginScreen extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            visibility: true,
            rightIcon: 'eye-off'
        }
    }

    visibilitySet(){
        
        let icon;

        if(this.state.rightIcon === 'eye-off')
            icon = 'eye'
        else
            icon = 'eye-off'

        this.setState({visibility: !this.state.visibility, rightIcon: icon})
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logoImage} source={require('../assets/logo.png')}/>
                </View>
                <View style={styles.fieldContainer}>
                    <TextInput 
                        underlineHeight={1}
                        underlineColor='grey'
                        underlineActiveColor='#208AEC'
                        leftIcon='email'
                        leftIconSize={20}
                        leftIconType='material'
                        placeholder='Email'
                        paddingLeft={32}
                        marginTop={10}
                        marginBottom={10}
                    />
                    <TextInput 
                        underlineHeight={1}
                        underlineColor='grey'
                        underlineActiveColor='#208AEC'
                        leftIcon='lock'
                        leftIconSize={20}
                        leftIconType='material'
                        rightIcon={this.state.rightIcon}
                        rightIconSize={20}
                        rightIconType='material'
                        placeholder='Password'
                        paddingLeft={32}
                        marginTop={10}
                        marginBottom={10}
                        onPressRightIcon={this.visibilitySet.bind(this)}
                        secureTextEntry={this.state.visibility}
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonLogin} onPress={()=>{this.props.navigation.push('About')}}>
                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 25
    },
    logoContainer: {
        marginTop: 40,
        alignItems: 'center'
    },
    logoImage:{
        width : DEVICE.width * 1,
        height: DEVICE.height * 0.2,
        
    }, 
    inputField: {
        borderBottomColor: '#208AEC',
        borderBottomWidth: 2,
        marginHorizontal: 40,
        marginVertical: 20
    },
    fieldContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainer:{
        flex: 1,
        marginVertical: 20,
        alignItems: 'center',
    },
    buttonLogin: {
        backgroundColor: '#3DB9FF',
        width: DEVICE.width * 0.8,
        height: DEVICE.height * 0.07,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius : 6,
        shadowColor: '#3DB9FF',
        shadowOpacity: 0.5,
        shadowOffset: {width:5,height:5}
    },
    buttonText: {
        color: '#FFFFFF',
        fontWeight: 'bold',
    }
})