import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions, Picker, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {FETCH_REQUEST2, FETCH_SUCCESS2, FETCH_FAILED2} from '../src/constants/actionTypes'
import Axios from 'axios'

import News from '../component/News'

const DEVICE = Dimensions.get('window')
const  Separator = () => (
    <View style={styles.itemSeparator}/>
)

export default class NewsScreen extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            isRefresh: false
        }
    }

    async fetchData(){
        try{
            this.props.dispatch({
                type: FETCH_REQUEST2
            })
            const data = await Axios.get("https://newsapi.org/v2/everything?q=corona&apiKey=d45346e9344442ac93480de6bbc86d7c")
            this.props.dispatch({
                type: FETCH_SUCCESS2,
                payload: data.data.articles
            })
            this.setState({isRefresh: false})
        }catch(err){
            this.props.dispatch({
                type: FETCH_FAILED2,
                payload: err,
            })
            console.log(err)
            this.setState({isRefresh: false})
        }
    }

    componentDidMount(){
        this.fetchData()
    }
    
    render(){

        const {news} = this.props;
        
        let arrNews = []
        if(news != undefined){
            arrNews = news.newsData
        }

        return(
            <View style={styles.container}>
                <Text style={styles.headerTitle}><Text style={styles.innerTitle}>Corona</Text> Global News</Text>
                <View style={styles.newsContainer}>
                    <FlatList
                        style={styles.FlatList}
                        refreshing={this.state.isRefresh} 
                        onRefresh={()=>{this.setState({isRefresh: true}); this.fetchData()}} 
                        ItemSeparatorComponent={Separator} 
                        data={arrNews} 
                        keyExtractor={(item) => {return item.id}} 
                        renderItem={(item) => <News navigation={this.props.navigation} id={item.index} url={item.item.urlToImage} title={item.item.title} desc={item.item.description} source={item.item.source.name} date={item.item.publishedAt}/>}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 40,
        paddingHorizontal: 0,
    },
    itemSeparator:{
        borderColor: 'grey',
        borderWidth: 0.5,
        marginVertical: 30
    },
    headerTitle:{
        fontWeight: 'bold',
        fontSize: 28,
        textAlign: 'center',
        marginBottom: 10
    },
    innerTitle:{
        color: 'red'
    },
    FlatList:{
        height: DEVICE.height * 0.85
    }
})