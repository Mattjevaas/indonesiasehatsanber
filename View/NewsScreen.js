import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions, Picker,Linking } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


const DEVICE = Dimensions.get('window')

export default class NewsScreen extends React.Component{

    constructor(props){
        super(props)
    }

    clickUrl(url){
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log("Don't know how to open URI: " + url);
            }
        })
    }

    render(){

        const {news} = this.props;

        let singleNews = null
        if(news != undefined){
            singleNews = news.newsData[this.props.route.params.id]
        }


        return(
            <View style={styles.container}>
                <View styles={styles.newsContainer}>
                    <Text style={styles.headerTitle}>{singleNews.title}</Text>
                    <Image style={styles.imageNews} source={{uri: singleNews.urlToImage}}/>
                    <View style={styles.authorContainer}>
                        <Text style={styles.headlineAuthor}>Author: {singleNews.author}</Text>
                        <Text style={styles.headlineSource}>Source: {singleNews.source.name} {singleNews.publishedAt}</Text>
                    </View>
                    <Text style={styles.contentNews}>{singleNews.content}</Text>
                    <TouchableOpacity onPress={()=>{this.clickUrl(singleNews.url)}}>
                        <Text style={styles.urlNews}>url: {singleNews.url}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    newsContainer: {

    },
    headerTitle :{
        fontWeight: 'bold',
        textAlign: 'justify',
        fontSize: DEVICE.width * 0.05,
        margin: 20
    },
    imageNews: {
        width: DEVICE.width * 0.9,
        height: DEVICE.height * 0.3,
        margin: 20
    },
    headlineSource: {
        fontWeight: '100',
        fontSize : DEVICE.width * 0.03,
        marginVertical: 5,
    },
    headlineAuthor: {
        fontWeight: '300',
        fontSize : DEVICE.width * 0.03,
        marginVertical: 5,
    },
    authorContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20

    },
    contentNews: {
        textAlign: 'justify',
        margin: 20
    },
    urlNews:{
        fontWeight: '200',
        color: 'blue',
        margin: 20
    }
})