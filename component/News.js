import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions, Picker } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const DEVICE = Dimensions.get('window')

export default class News extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity style={styles.newsContainer} onPress={()=>{this.props.navigation.push('News',{id: this.props.id} )}}>
                    <Image style={styles.imageHeadline} source={{uri: this.props.url}}/>
                    <View style={styles.descContainer}>
                        <Text style={styles.headlineTitle}>{this.props.title}</Text>
                        <Text style={styles.headlineDesc}>{this.props.desc}</Text>
                        <Text style={styles.headlineSource}>Source: {this.props.source} {this.props.date}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    imageHeadline: {
        width: DEVICE.width * 0.2,
        height: DEVICE.height * 0.1,
        aspectRatio: 3/3
    },
    newsContainer: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: DEVICE.width * 0.2
    },
    descContainer:{
        marginHorizontal: 20,
        justifyContent: 'center'
    },
    headlineTitle: {
        fontWeight: 'bold',
        fontSize : DEVICE.width * 0.05,
        marginVertical: 5,
        textAlign: 'justify',
    },
    headlineDesc: {
        marginVertical: 5,
        textAlign: 'justify',
        
    },
    headlineSource: {
        fontWeight: '100',
        fontSize : DEVICE.width * 0.03,
        marginVertical: 5
    }
})