import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import AnimateNumber from 'react-native-countup'

const DEVICE = Dimensions.get('window')

export default class Box extends React.Component{

    constructor(props){
        super(props)
    }

    toInt(value){

        if(value != 0){
            let newVal = value.replace(",","")
            return parseInt(newVal)
        }

        return 0
    }

    render(){
        return(
            <View style={styles.box}>
                <View style={styles.container1}>
                    <View style={styles.container2}>
                        <Icon name={this.props.iconName} size={20} color={this.props.iconColor}/>
                        <Text>{this.props.title}</Text>
                    </View>
                    <AnimateNumber style={styles.numberBox} countBy={1000} value={this.toInt(this.props.number)} interval={1} /> 
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    box: {
        marginVertical: 20,
        marginHorizontal: 30,
        backgroundColor: 'white',
        borderRadius: 9,
        shadowColor: 'black',
        shadowOpacity: 0.2,
        shadowOffset: {width:1,height:1},
        width: DEVICE.width * 0.28,
        height: DEVICE.height * 0.15,
        justifyContent: 'center',
        elevation: 2
    },
    container1: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container2: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    numberBox: {
        fontWeight: 'bold',
        fontSize: 25,
        marginVertical: 20
    }
})