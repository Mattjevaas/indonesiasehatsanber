import {combineReducers} from 'redux';
import coronaReducer from './coronaReducer';
import newsReducer from './newsReducer';

const reducers = combineReducers({
    corona :  coronaReducer,
    news : newsReducer
})

export default reducers