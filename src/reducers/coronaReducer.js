import {
    FETCH_REQUEST,
    FETCH_SUCCESS,
    FETCH_FAILED
} from '../constants/actionTypes'

const initialState = {
    isLoading: false,
    coronaData : [],
    isFailed: false
}

const coronaReducer = (state=initialState,action) => {
    switch(action.type){
        case FETCH_REQUEST: 
            return {
                ...state,
                isLoading: true
            }
        case FETCH_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isFailed: false,
                coronaData: [
                    ...action.payload
                ]
            }
        case FETCH_FAILED:
            return {
                ...state,
                isLoading: false,
                isFailed: action.payload,
            }
        default:
            return state
    }
}

export default coronaReducer;