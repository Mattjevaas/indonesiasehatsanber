import {
    FETCH_REQUEST2,
    FETCH_SUCCESS2,
    FETCH_FAILED2
} from '../constants/actionTypes'

const initialState = {
    isLoading: false,
    newsData : [],
    isFailed: false
}

const newsReducer = (state=initialState,action) => {
    switch(action.type){
        case FETCH_REQUEST2: 
            return {
                ...state,
                isLoading: true
            }
        case FETCH_SUCCESS2:
            return {
                isLoading:false,
                isFailed: false,
                newsData: [
                    ...action.payload
                ]
            }
        case FETCH_FAILED2:
            return {
                ...state,
                isLoading: false,
                isFailed: action.payload,
            }
        default:
            return state
    }
}

export default newsReducer;