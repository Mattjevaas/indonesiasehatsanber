import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Provider, connect } from 'react-redux';
import store from './src/store/store'

import Login from './view/LoginScreen'
import About from './view/AboutScreen'
import Main from './view/MainScreen'
import Global from './view/GlobalScreen'
import News from './view/NewsScreen'

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const GlobalStack = createStackNavigator();

const MainContainer = connect(state => ({corona: state.corona}))(Main);
const GlobalContainer = connect(state => ({news: state.news}))(Global);
const NewsContainer = connect(state => ({news: state.news}))(News);

const GlobalStacked = () => (

  <GlobalStack.Navigator>
    <GlobalStack.Screen name="Global" component={GlobalContainer} options={{headerShown: false}}/>
    <GlobalStack.Screen name="News" component={NewsContainer} options={{headerTitle: 'News Detail'}}/>
  </GlobalStack.Navigator>
)


const MainStack = () => (

  <Drawer.Navigator>
    <Drawer.Screen name="Update" component={MainContainer} />
    <Drawer.Screen name="Global" component={GlobalStacked} />
    <Drawer.Screen name="About Us" component={About} />
    <Drawer.Screen name="Logout" component={Login} />
  </Drawer.Navigator>

)
export default class App extends React.Component {
  render(){
    return (
      <Provider store={store()}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName='Login'>
            <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
            <Stack.Screen name="About" component={About} options={{headerShown: false}}/>
            <Stack.Screen name="Main" component={MainStack} options={{headerShown: false}}/>
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

